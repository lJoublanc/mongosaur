package mongosaur

import mongosaur.mongodb.commands
import scodec.Codec
import scodec.bits.BitVector
import scodec.protocols.bson.BSONCodec

/** A typeclass for creating a codec that encodes the projection operation
  * used in e.g. `OP_QUERY` at wire level or `Find` at command level.
  */
trait Projection[A] {
  def codec: BSONCodec[Unit]
}

object Projection extends LowPrioProjection {
  import scodec.codecs.ignore

  /** Construct a return fields selector with from fields of `A`. */
  def apply[A](implicit rfs: Projection[A]) = rfs

  /** Use this to explicitly omit the field selector for any `A`. */
  def omit[A] =  new Projection[A] { val codec = liftToBSONCodec(ignore(0)) }

  /** If the reply type is inferred from a command, return all fields by default */
  implicit def fromReply[A](implicit rep: commands.Command.Reply[_,A]): Projection[A] = 
    new Projection[A]{
      val codec: BSONCodec[Unit] = liftToBSONCodec(ignore(0))
    }
}

trait LowPrioProjection {
  import shapeless._
  import shapeless.ops.hlist.LeftFolder
  import shapeless.ops.record.Keys
  import scodec.Attempt
  import scodec.codecs.{constant}
  import scodec.protocols.bson.int32

  protected def liftToBSONCodec[A](codec: Codec[A]) = 
    codec.asInstanceOf[BSONCodec[A]]

  object ReturnFieldSelector extends Poly2 {
    implicit def any[K : Witness.Aux]: Case.Aux[Attempt[BitVector],K,Attempt[BitVector]] =
      at { (maybeAcc,_) =>
        for { acc <- maybeAcc
              one <- int32[K].encode(1)
        } yield acc ++ one
      }
  }

  private type ID = Witness.`'_id`.T

  /** Note that `_id` must be [[https://stackoverflow.com/questions/9598505/mongoose-retrieving-data-without-id-field explicitly excluded]]. */
  implicit def subset[A, L <: HList, K <: HList, P <: HList](
      implicit
      labels: LabelledGeneric.Aux[A,L],
      noId: NotContainsConstraint[L,ID],
      keys: Keys.Aux[L,K],
      folder: LeftFolder.Aux[K,Attempt[BitVector],ReturnFieldSelector.type,Attempt[BitVector]]
  ): Projection[A] = new Projection[A] {
    lazy val codec: BSONCodec[Unit] = liftToBSONCodec {
      constant {
        val values = 
          for { id <- int32[ID].encode(0)
                acc <- keys().foldLeft(Attempt successful BitVector.empty)(ReturnFieldSelector)
          } yield id ++ acc
        values.require
      }
    }
  }
}

