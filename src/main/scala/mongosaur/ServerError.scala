package mongosaur

/** Used to tag errors originated from a MongoDB server, in response to a 
  * message we have sent it.
  * These extend throwable so they can be used in an `cats.ErrorMonad` 
  * e.g. `cats.effect.IO` or `fs2.Stream`.
  * It must be in the form of a BSON Document (not a wire-level error).
  * @see [[https://stackoverflow.com/questions/27465951/how-to-avoid-scalas-case-class-default-tostring-function-being-overridden/27467406]]
  */
abstract class ServerError(message: String) 
extends Throwable(message) 
with Product 
with Serializable {
  override def toString = scala.runtime.ScalaRunTime._toString(this)
}
