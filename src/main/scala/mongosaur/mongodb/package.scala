package mongosaur

import cats.MonadError
import fs2.io.tcp
import fs2.Chunk._

package object mongodb {
  //TODO: Move this into mongosaur package.
  case class CodecException(error: scodec.Err) extends Exception(error.messageWithContext)

  implicit class BsonSocket[F[_]](sock : tcp.Socket[F])(implicit F : MonadError[F,Throwable]) {
    import scodec.bits.ByteVector
    import scodec.{Encoder,Decoder}
    import cats.syntax.flatMap._
    import fs2._

    import scala.concurrent.duration._

    final val BUFF_SIZE_BYTES = 256 * 1024 //TODO get from socket

    /** Utility function for serializing and writing to socket in one step.
      * @tparam A a type with an implicit mongodb wire `scodec.Codec` available.
      * TODO: This will fail for messages larger than BUFF_SIZE_BYTES.
      */
    def mongoRead[A: Decoder](timeout: Option[FiniteDuration] = Some(30 seconds)):
        Stream[F,A] = {

      Stream eval {
        sock
          .read(BUFF_SIZE_BYTES,timeout)
          .flatMap {
            _.fold[F[ByteVector]] {
              sock.remoteAddress flatMap { sock =>
                F raiseError new Error(s"$sock closed the remote connection.")
              }
            } {
              case Bytes(bs,off,len) => F pure ByteVector.view(bs,off,len)
              case ByteVectorChunk(bv) => F pure bv
              case b : ByteBuffer => F pure ByteVector.view(b.toByteBuffer)
              case x => F raiseError new Error(s"Expected Chunk[Byte] got $x")
            }
          } 
          .flatMap { bytes =>
            F fromEither { 
              Decoder[A].decodeValue(bytes.bits)
                .toEither
                .left.map(CodecException)
            }
          }
      }
    }


    /** Utility function for reading and deserializing from socket in one step.
      * @tparam A a type with an implicit mongodb wire `scodec.Codec` available.
      */
    def mongoWrite[A: Encoder](a: A)(timeout: Option[FiniteDuration] = Some(30 seconds))
        : Stream[F,Unit] =
      Stream eval {
        F fromEither { 
          (Encoder[A] encode a)
            .toEither
            .left.map(CodecException)
        } flatMap { bits =>
          sock.write(ByteVectorChunk(bits.bytes), timeout)
        }
      }
  }
}
