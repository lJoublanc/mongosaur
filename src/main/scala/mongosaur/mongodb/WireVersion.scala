package mongosaur.mongodb

trait WireVersion {

  val minWireVersion : Int

  val maxWireVersion : Int
}
