package mongosaur.mongodb

import scodec.Codec
import scodec.protocols.bson.BSONElement
import cats.{Id, Applicative, Foldable}
import fs2.Stream

import mongosaur.mongodb.commands._

trait Collection[F[_]] {

  def name : String

  def fullyQualifiedName: String
 
  def writeConcern: WriteConcern

  def readPreference: ReadPreference

  /** Run a command on this collection, with `fullCollectionName` set to 
    * `<collname>.\$cmd`
    */
  def command[Q, R](
      cmd : Q,
      rp : ReadPreference = readPreference
    )(
      implicit 
      command: Command.Reply[Q,R],
      enc: Codec[Q],
      dec: Codec[R]): Stream[F,R]

  /** Drops this collection. */
  def drop(implicit c: Codec[Drop], r: Codec[Drop.Reply]): Stream[F,Drop.Reply]

  /** Insert multiple documents.
    * @example {{{
    * scala> client.database("test").coll("test").insert(docs).reduceSemigroup
    * res0: Semigroup(ok: 1, 23, None, Some(List(WriteConcernError(123,"err"))))
    * }}}
    * TODO : if C : Monad, then use command insert, else bulk insert ??. */
  def insert[C[_] : Applicative : Foldable, A](
      documents : C[A])(
      implicit 
      codec: BSONElement[A],
      rep: Codec[Insert.Reply]
      ) : Stream[F,Insert.Reply]

  /** Insert one document. */
  def insert[A](a : A)(
      implicit c: BSONElement[A],
      rep: Codec[Insert.Reply]
    ): Stream[F,Insert.Reply] =
    insert[Id,A](a)(implicitly[Applicative[Id]],implicitly[Foldable[Id]],c,rep)

  /** Find multiple documents.
    * If called with empty argument list, return all documents in the collection.
    * @usecase def find[A](query: Any): Stream[F,A]
    * @see Use [[Collection#command]] with [[commands.Find]] if you need more
    *      granular control over the query.
    * @note do not pass an empty case class. To return all elements, call find without no value arguments.
    */
  def find[A]: Collection.FindBuilder[F,A]
}

object Collection {
  import mongosaur.Projection

  protected[mongodb] def apply[F[_]](
      collName: String,
      wc: WriteConcern,
      rp: ReadPreference,
      client: Client[F],
      db: Database[F]): Collection[F] = new Collection[F] { coll =>
    import cats.{Applicative,Foldable}
    import cats.syntax.foldable._

    final private lazy val DB_CMD = db.name + ".$cmd"

    def name = collName

    def fullyQualifiedName = db.name + "." + name

    def writeConcern = wc

    def readPreference = rp

    def command[Q, R](
      cmd: Q,
      rp: ReadPreference = coll.readPreference
    )(
      implicit 
      command: Command.Reply[Q,R],
      enc: Codec[Q],
      dec: Codec[R]) = client.command(cmd, rp, fullCollectionName = DB_CMD)

    def drop(implicit req: Codec[Drop], rep: Codec[Drop.Reply]): Stream[F,Drop.Reply] =
      command(Drop(coll.name))

    def insert[C[_] : Applicative : Foldable, A](
        documents: C[A]
      )(
        implicit 
        req: BSONElement[A],
        rep: Codec[Insert.Reply]
      ): Stream[F,Insert.Reply] = {
      import scodec.protocols.bson.implicits._
      val docs = documents.toList
      command[Insert[A],Insert.Reply](
        Insert(
          coll.name,
          docs,
          coll.writeConcern.ordered,
          coll.writeConcern.toMongoWriteConcern,
          !coll.writeConcern.validate
        ),
      )
    }

    def find[A]: FindBuilder[F,A] = new FindBuilder[F,A]{
      def apply[Q](query: Q)(implicit Q: Codec[Q], A: Codec[A], returnFieldSelector: Projection[A]): Stream[F,A] = {
        client.command(query,rp,fullCollectionName = fullyQualifiedName)(Command[Q,A],returnFieldSelector,Q,A)
      }
    }
  }

  protected[Collection] trait FindBuilder[F[_],A] {
    def apply[Q](query: Q)(
        implicit 
        Q: Codec[Q],
        A: Codec[A],
        returnFieldSelector: Projection[A]): Stream[F,A] 
  } 
}
