package mongosaur.mongodb

trait Database[F[_]] {

  /** Name of the database. */
  def name : String

  /** The default write concern of the database. */
  def writeConcern: WriteConcern

  /** The default read preference of the database. */
  def readPreference: ReadPreference

  /** Return context for performing operations on a collection. */
  def collection(
    name : String,
    writeConcern: WriteConcern = writeConcern,
    readPrefernce: ReadPreference = readPreference
  ): Collection[F]
}

object Database {
  protected[mongodb] def apply[F[_]](
      dbName: String,
      wc: WriteConcern,
      rp: ReadPreference,
      client: Client[F]): Database[F] = new Database[F]{ db =>

    def name = dbName

    def readPreference = rp

    def writeConcern = wc

    def collection(
      collName: String,
      writeConcern: WriteConcern = writeConcern,
      readPrefernce: ReadPreference = readPreference
    ) : Collection[F] = Collection[F](collName,writeConcern,readPreference,client,db)
  }
}
