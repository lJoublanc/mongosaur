package mongosaur.mongodb

import java.net.{InetSocketAddress,URI}

/** @see [[https://docs.mongodb.com/manual/reference/connection-string/]] */
case class ConnectionString (uri : URI) {

  /** The protocol. Used to enable TLS etc. */
  def protocol : Option[String] = Option(uri.getScheme)

  protected lazy val userInfo = Option(uri.getUserInfo).map(_ split ":")

  /** The user, if specified. */
  def user : Option[String] = userInfo.flatMap(_.headOption)

  /** The password, if specified. */
  def password : Option[String] = userInfo.flatMap(_.tail.headOption)

  /** Server addresses. Port defaults to 27017 if not specified. */
  def servers : Seq[InetSocketAddress] = 
    for { authority <- Option(uri.getAuthority).toSeq
          servers = authority split "@" last;
          server <- servers split ","
          Array(host, optPort @ _*) = server split ":"
    } yield new java.net.InetSocketAddress(
      host,
      optPort.map(_.toInt).headOption.getOrElse(27017)
    )

  /** Sequence of parsed (parameter,value) pairs. */
  def queries : Seq[(String,String)] =
    for { queries <- Option(uri.getQuery).toSeq
          query <- queries split ","
          Array(param, value) = query split ":"
    } yield param -> value

  override def toString = s"ConnectionString($uri)"
}

object ConnectionString {
  def apply(uri : String = "mongodb://localhost") = 
    new ConnectionString(new URI(uri))
}

