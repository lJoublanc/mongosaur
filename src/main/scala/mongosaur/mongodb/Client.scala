package mongosaur.mongodb

import java.nio.channels.AsynchronousChannelGroup 
import java.util.concurrent.Executors
import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

import cats.syntax.functor._
import cats.syntax.alternative._
import cats.instances.option._
import cats.instances.list._
import fs2._
import fs2.concurrent.Signal
import fs2.io.tcp
import cats.effect.{Timer,ConcurrentEffect}
import cats.effect.concurrent.Ref
import scodec.Codec

import mongosaur.mongodb.commands._
import mongosaur.Projection

trait Client[F[_]] extends WireVersion {
  import Client._

  /** Configuration, possibly parsed from [[connectionString]]. */
  val config : Configuration

  /** Atomic counter that returns the next request id. */
  protected def requestId(): F[Int]

  /** The default read preference. Override this by declaring a local 
    * variable with the same name.
    * Defaulted to [[ReadPreference.Primary]], per spec. */
  def readPreference : ReadPreference = ReadPreference.Primary

  /** The write concern. Determines whether writes are ACK'ed etc.
    * @see [[https://docs.mongodb.com/manual/reference/write-concern]]
    */ 
  def writeConcern: WriteConcern = WriteConcern.default

  /** Topology of the MongoDB cluster, updated at least every [[Client.Configuration.heartbeatFrequency]].
    * Emits `None` when the topology can not be determined, e.g. if
    * servers are down or network fails.
    */
  def topology: Signal[F,Option[Topology]]

  /** Select a server for reading/writing according to the read 
    * preference, following [[https://github.com/mongodb/specifications/blob/master/source/server-selection/server-selection.rst#rules-for-server-selection the rules]].
    * @param rp The read preference, ignored for direct connections.
    */
  def selectServer(rp: ReadPreference): Stream[F,(tcp.Socket[F],Server)]

  /** Equivalent of mongo shell's `dbCommand`. 
    * Runs one of the documented [[https://docs.mongodb.com/manual/reference/command/ database commands]].
    *
    * @param cmd A case-class with the fields of a [[https://docs.mongodb.com/manual/reference/command/ database command]].
    * @param rp The read-preference. Defaults to [[ReadPreference.Primary]].
    *           [[https://github.com/mongodb/specifications/blob/master/source/server-selection/server-selection.rst#id28 it is up to the user to use an appropriate read preference, e.g. not calling `renameCollection` with a `mode` of 'Secondary']]
    * @param returnFieldSelector If passed explicitly, will only return the fields in `A`.  By default all fields are returned.
    * @param serverSelector An optional function to override the default server selection process.
    */
  def command[Q,A](
    cmd : Q,
    rp : ReadPreference = ReadPreference.Primary,
    serverSelector: ReadPreference => Stream[F,(tcp.Socket[F],Server)] = selectServer,
    fullCollectionName: String = DB_CMD
  )(implicit
    command: Command.Reply[Q,A],
    returnFieldSelector: Projection[A],
    enc: Codec[Q],
    dec: Codec[A],
  ): Stream[F,A]

  /** Database context for manipulating a specific database. */
  def database(
    name : String,
    wc : WriteConcern = writeConcern,
    rp : ReadPreference = readPreference): Database[F]

  val connectionString : ConnectionString
}

object Client {

  type SrvMap[F[_]] = Map[tcp.Socket[F],Option[Server]]

  private val DB_CMD = "admin.$cmd"

  def apply[F[_]](
      cs : ConnectionString)(
      implicit 
      acg: AsynchronousChannelGroup = AsynchronousChannelGroup.withThreadPool(Executors.newCachedThreadPool) , //TODO should be separate for monitors and general.
      ec: ExecutionContext = ExecutionContext.global,
      timer: Timer[F],
      F: ConcurrentEffect[F],
    ): Stream[F,Client[F]] = {

    /* @see [[https://github.com/mongodb/specifications/blob/master/source/mongodb-handshake/handshake.rst Handshake Specification]] */
    def isMaster(sock : tcp.Socket[F], requestId : Int, handshake : Boolean = false)
        : Stream[F,Option[Server]] = {
      import mongosaur.mongodb.wire.{OP_QUERY,OP_REPLY}
      import mongosaur.mongodb.commands.{Handshake,IsMaster}
      import mongosaur.mongodb.commands.IsMaster._
      import mongosaur.mongodb.commands.Handshake.{Client => Cli}
      import mongosaur.mongodb.commands.Handshake.Client._
      import java.lang.System.getProperty

      val handshakeMsg =  
        Handshake(true,
          Cli(//TODO check it doesn't exceed 512 bytes
            Application(cs.queries.collectFirst{ case ("appname",a) => a }.getOrElse("unknown")), //TODO : check 128 byte limit.
            Driver("mongosaur","0.1.0"),
            Os("unknown", //TODO : type
               getProperty("os.name","unknown"),
               getProperty("os.arch","unknown"),
               getProperty("os.version","unknown")
            ),
            List("vendor","name","version").map(x => getProperty("java.vm." + x,"unknown")).mkString(" ")
          )
        )

      //TODO : importing implicits._ and trying to derive codec causes SO - possibly because derivedCodec override
      implicit val (hsCodec,imCodec,saCodec) = {
        import scodec.protocols.bson.implicits._
        (Codec[Handshake], Codec[IsMaster], Codec[Standalone])
      }

      //TODO : large amount of duplication with `command` here.
      for {
        start <- Stream eval timer.clock.monotonic(NANOSECONDS)
        _ <- if (handshake) 
               sock.mongoWrite(OP_QUERY[Handshake,IsMaster.Standalone](handshakeMsg, DB_CMD, requestId, 1))(Some(30 seconds))
             else 
               sock.mongoWrite(OP_QUERY[IsMaster,IsMaster.Standalone](IsMaster(true), DB_CMD, requestId, 1))(Some(30 seconds))
        rep <- sock.mongoRead[OP_REPLY[Standalone]](timeout = Some(30 seconds))
        end <- Stream eval timer.clock.monotonic(NANOSECONDS)
        rtt = end - start
        doc <- Stream.emits(rep.documents).covary[F].rethrow.rethrow.last
      } yield doc map (standalone => Server(rtt nanos, standalone))
    }

    def mkServers(counter : Ref[F,Int]) : Stream[F,SrvMap[F]] =
      Stream.emits(cs.servers).covary[F]
        .map { addr =>
          Stream eval (counter modify increment) flatMap { i =>
            Stream resource tcp.client(addr,keepAlive = true) flatMap { generalSock =>
              Stream resource tcp.client(addr,keepAlive = true) flatMap { monitorSock =>
                val default =
                  Stream emit (generalSock -> None)
                val hello = 
                  isMaster(generalSock,i,handshake = true) tupleLeft generalSock 
                val updates = 
                    Stream.awakeDelay[F](10 seconds) >> {
                      Stream eval (counter modify increment) flatMap { i => 
                        isMaster(monitorSock,i) tupleLeft generalSock
                      }
                    }
                default ++ hello ++ updates
              }
            }
          } repeat //TODO : use attempts instead?
        }
        .parJoinUnbounded
        .scan(Map.empty: SrvMap[F]){
          case (ss,(k,sOpt)) =>
            val v2 = 
              sOpt map { next =>
                ss(k).fold(next) { prev =>
                  val alpha = 0.2
                  val ewmaRtt = alpha * prev.rtt + (1.0 - alpha) * next.rtt
                  Server(ewmaRtt, next.reply)
                }
              }
            ss + (k -> v2)
        }

    def inferTopology(servers : SrvMap[F]) : Option[Topology] = 
      servers.toSeq.map{ case (k,vOpt) => vOpt tupleLeft k}.toList.unite match {
        case standalone :: Nil => Some(Topology.Single(standalone))
        case Nil => None
        case cluster => ???
      }
       

    /* @see [[https://github.com/mongodb/specifications/blob/master/source/auth/auth.rst Authentication Specification]] */
    def authenticate(server : Server, credential : Credential) : F[Unit] = 
      F.unit //TODO : perform authentication.

    Stream.eval(Ref.of[F,Int](Int.MinValue)) flatMap { counter =>
      mkServers(counter) map inferTopology hold None map { top =>
        new Client[F] { client =>

          val minWireVersion = 4

          val maxWireVersion = 4

          val connectionString = cs

          val config = {
            import shapeless.{Generic,LabelledGeneric,Poly1,Witness}
            import shapeless.labelled.FieldType
            val defaults = LabelledGeneric[Configuration].to(Configuration())
            val values = connectionString.queries.toMap

            implicit object ParseConfigValue extends Poly1 {
              implicit def f[S <: Symbol](implicit wit : Witness.Aux[S]):
                  Case.Aux[FieldType[S,FiniteDuration],FiniteDuration] = 
                at { default : FiniteDuration =>
                  val name = wit.value.name
                  values get name map { x =>
                    if (name endsWith "MS")
                      FiniteDuration(x.toInt, MILLISECONDS)
                    else if (name endsWith "Seconds")
                      FiniteDuration(x.toInt, SECONDS)
                    else 
                      throw new NumberFormatException(s"Can not determine the the time units of '$name' form name.")
                  } getOrElse default
                }
            }

            Generic[Configuration] from (defaults map ParseConfigValue)
          }
            

          protected val requestId: F[Int] = counter modify increment

          def selectServer(rp: ReadPreference): Stream[F,(tcp.Socket[F],Server)] = {
            val timeout = 
              Stream
                .raiseError(ServerSelectionTimeout)
                .covary[F]
                .delayBy(config.serverSelectionTimeout)
                .drain

            val chosenServer = topology.discrete.unNone
              .map {
                case direct: Topology.Single[F @unchecked] => List(direct.server)
                case nondirect => rp.isEligible[F](nondirect)
              }
              .map{ ss =>
                // Subtract A from formula to get: 0 ≤ RTT - A ≤ localThreshold
                import config.localThreshold
                val a = ss.foldLeft(Duration.Inf : Duration){
                  case (minRtt,(_,Server(rtt,_))) => minRtt min rtt
                }
                ss.filter(_._2.rtt - a <= localThreshold)
              }
              .find(_.nonEmpty)
              .map(ss => ss(scala.util.Random.nextInt() % ss.size)) //TODO make random pure. Change List -> Vector?

            timeout mergeHaltR chosenServer
          }

          lazy val topology: Signal[F,Option[Topology]] = top

          def command[Q,R](
            cmd : Q,
            rp : ReadPreference = ReadPreference.Primary,
            serverSelector: ReadPreference => Stream[F,(tcp.Socket[F],Server)] = selectServer,
            fullCollectionName: String = DB_CMD
          )( 
            implicit 
            command: Command.Reply[Q,R],
            returnFieldSelector: Projection[R],
            enc: Codec[Q],
            dec: Codec[R],
          ): Stream[F,R] = {
            import mongosaur.mongodb.wire._
            val timeout = Some(30 seconds)
            val slaveOk = rp.slaveOK

            /* Recursively requests more data til cursorId == 0 */
            def getMore(cursor: Long, sock: tcp.Socket[F]): Stream[F,R] = 
              for { reqId <- Stream eval requestId
                    _ <- sock.mongoWrite(OP_GET_MORE(cursor,fullCollectionName,reqId,1))(timeout) //TODO return > 1
                    rep <- sock.mongoRead[OP_REPLY[R]](timeout)
                    repId = rep.header.responseTo
                    _ = require(repId == reqId,ReqRepMismatchError(reqId,repId))
                    docs = Stream.emits(rep.documents).covary[F].rethrow.rethrow
                    doc <- if (rep.cursorId == 0) docs 
                           else docs ++ getMore(rep.cursorId,sock)
              } yield doc

            def killCursor(cursor: Long, sock: tcp.Socket[F]): F[Unit] =
              Stream
                .eval(requestId)
                .flatMap { reqId =>
                  sock.mongoWrite(OP_KILL_CURSORS(reqId,cursor))(timeout)
                }
                .compile
                .drain

            for { reqId <- Stream eval requestId
                  s <- serverSelector(rp)
                  (sock,_) = s
                  _ <- sock.mongoWrite(OP_QUERY[Q,R](cmd,fullCollectionName,reqId,1,slaveOk = slaveOk))(timeout)
                  rep <- sock.mongoRead[OP_REPLY[R]](timeout)
                  repId = rep.header.responseTo
                  _ = require(repId == reqId,ReqRepMismatchError(reqId,repId))
                  doc1 = Stream.emits(rep.documents).covary[F].rethrow.rethrow
                  doc <- if (rep.cursorId == 0) doc1 
                         else doc1 ++ Stream.bracket(F pure rep.cursorId)(killCursor(_,sock)).flatMap(getMore(_,sock))
            } yield doc
          }

          def database(
              dbName: String,
              wc: WriteConcern = writeConcern,
              rp: ReadPreference = readPreference
            ): Database[F] = Database[F](dbName,wc,rp,client)
            
        }
      }
    }
  }

  /** @see [[https://github.com/mongodb/specifications/blob/master/source/server-selection/server-selection.rst#mongoclient-configuration]] */
  case class Configuration(
    localThreshold: FiniteDuration = 15 milliseconds,
    serverSelectionTimeout: FiniteDuration = 30 seconds,
    heartbeatFrequency: FiniteDuration = 10 seconds,
  ) {
    import Configuration._
    require(heartbeatFrequency >= minHeartBeatFrequency)
  }

  object Configuration {
    val idleWritePeriod: FiniteDuration = 10 seconds
    val smallestMaxStaleness: FiniteDuration = 90 seconds
    val minHeartBeatFrequency : FiniteDuration = 500 milliseconds
  }

  /** Used to describe the cluster topology.
    * @param servers List of MongoDB servers.
    */
  sealed trait Topology

  object Topology {
    case class Single[F[_]] (
      server : (tcp.Socket[F], Server),
    ) extends Topology 
  }

  protected[mongodb] case class Server (
    rtt : Duration,
    reply : commands.IsMaster.Reply
  )

  private case class Credential(
    userName: String,
    source: String,
    password: String,
    mechanism: String,
    //mechanismProperties : ???
  )

  private val increment = (i : Int) => { val j = i + 1; (j,j) }
  case object ServerSelectionTimeout extends Error

  case class ReqRepMismatchError(reqId: Int, repId: Int) 
  extends mongosaur.ServerError(s"Expected reply to #$reqId, got $repId")
}

