package mongosaur.mongodb

import scodec.protocols.bson.types._
import scodec.codecs.{cstring,bits,constant,listOfN,discriminatorFallback}
import scodec.bits.BitVector
import scodec.Codec
import scodec.codecs.StringEnrichedWithCodecContextSupport

import mongosaur.mongodb.commands.Command.Error
import mongosaur.Projection

/** The MongoDB Wire Protocol.
  * This package provides classes and codecs for mongodb messages.
  * _In addition_ to BSON for documents, MongoDB has it's own 
  * [[https://docs.mongodb.com/v3.4/reference/mongodb-wire-protocol/#messages-types-and-formats binary protocol specification]]
  * which is what this package implements.
  * See [[https://emptysqua.re/blog/driver-features-for-mongodb-3-6/#op-msg this blog post]]
  * for an overview of the different wire protocol versions.
  */
package object wire {

  /** The first element of MsgHeader, calculated implicitly */
  private def messageLength[A](len : Codec[Int], value : Codec[A]) : Codec[A] = {
    val size : Int = len.sizeBound.exact.map(_ / 8L).get.toInt
    scodec.codecs.variableSizeBytes(len.xmap(_ - size, _ + size), value)
  }

  /** The message header.
    * Fields which are implicit are added during encoding i.e. `messageLength`
    * and `opCode`, and don't appear as values.
    * @param responseTo Used to correlate a response with a request. 
    *                   Defaults to 0 (as it's only populated by server)
    */
  case class MsgHeader(requestID: Int, responseTo: Int = 0)

  object MsgHeader {
    implicit val codec : Codec[MsgHeader] = (
      ("requestID" | int32) :: 
      ("responseTo" | int32)
    ).as[MsgHeader]
  }

  import OP_REPLY.OpReplyError

  /** @see [[https://docs.mongodb.com/manual/reference/mongodb-wire-protocol/#op-reply]] */
  case class OP_REPLY[R] (
    header: MsgHeader,
    responseFlags: BitVector,
    cursorId: Long,
    startingFrom: Int,
    documents: List[Either[OpReplyError,Either[Error,R]]]
  ) {
    final def numberReturned = documents.size

    def cursorNotFound: Boolean = responseFlags(0)

    def queryFailure: Boolean = responseFlags(1)

    def awaitCapable: Boolean = responseFlags(3)
  }

  object OP_REPLY {

    case class OpReplyError(`$err`: String, code: Int) 
    extends mongosaur.ServerError(s"($code): ${`$err`}")

    object OpReplyError {
      implicit val codec = {//FIXME!!!! this is evaluating to null, if adding the type `:Codec[Error]`.
        import scodec.protocols.bson.implicits._
        Codec[OpReplyError] 
      }
    }

    implicit def codec[R : Codec]: Codec[OP_REPLY[R]] = {
      import cats.implicits._

      // This helper is used to avoid operator precedence probs with scoping.
      def secondHalf(queryFailure: Boolean) = {
        ("cursorId"     | int64 ) :: ("startingFrom" | int32 ) :: listOfN[Either[OpReplyError,Either[Error,R]]](
          "numberReturned" | int32,
          if (queryFailure)
              "document (OP_REPLY Error)" | OpReplyError.codec
                .widenOpt[Either[OpReplyError,Either[Error,R]]](_.asLeft[Either[Error,R]],_.swap.toOption)
          else
            discriminatorFallback[R,Error](
              "document[ok=1]" | Codec[R],
              "document[ok=0]" | Error.codec,
            ).widenOpt[Either[OpReplyError,Either[Error,R]]](_.swap.asRight[OpReplyError],_.toOption.map(_.swap))
        ).withContext("documents")
      }

      messageLength( "length" | int32,
        ( MsgHeader.codec
          :: ("opcode"       | constant(int32.encode(1).require))
          :: ("responseFlags" | 
               bits(32).xmap[BitVector](_.reverseByteOrder.reverse,_.reverse.reverseByteOrder)
             ).flatPrepend(bits => secondHalf(bits(1)))
        ).as[OP_REPLY[R]]
      )
    }.withContext("OP_REPLY")
  }

  case class OP_GET_MORE private(
    header: MsgHeader,
    fullCollectionName: String,
    numberToReturn: Int,
    cursorID: Long
  )

  object OP_GET_MORE {
    def apply(
      cursorId: Long,
      fullCollectionName: String,
      requestId: Int,
      numberToReturn: Int) =
    new OP_GET_MORE(
      MsgHeader(requestId),
      fullCollectionName,
      numberToReturn,
      cursorId)

    implicit val codec = 
      messageLength("length" | int32,
        MsgHeader.codec :: 
        ("opcode"       | constant(int32.encode(2005).require)) ::
        constant(0) ::
        ("fullCollectionName" | cstring) ::
        ("numberToReturn" | int32) ::
        ("cursorId" | int64)
      ).as[OP_GET_MORE]
  }

  case class OP_KILL_CURSORS private(
    header: MsgHeader,
    cursorIDs: List[Long]
  )

  object OP_KILL_CURSORS {
    def apply(requestId: Int, cursorId: Long*) =
      new OP_KILL_CURSORS(
        MsgHeader(requestId),
        List(cursorId: _*)
      )

    implicit val codec = 
      messageLength("length" | int32,
        MsgHeader.codec ::
        ("opcode" | constant(int32.encode(2007).require)) ::
        constant(0) ::
        listOfN("#OfCursorIds" | int32, "cursorIDs" | int64)
      ).as[OP_KILL_CURSORS]
  }

  /** @tparam P The query will request the fields of `P` when encoded. */
  sealed case class OP_QUERY[Q,P] (
    header: MsgHeader,
    flags: BitVector,
    fullCollectionName: String,
    numberToSkip: Int,
    numberToReturn: Int,
    document: Q,
    //returnFieldsSelector: P
  )

  object OP_QUERY {
    /** `OP_QUERY` wire message. If the parameter `R` is specified, `returnFieldSelector`
      * will be appended to the message on encoding, only requesting the fields names in
      * `R`. If the parameter is omitted, all fields will be returned.
      * @tparam R The return type will be restricted to these fields, if present.
      * @param numberToReturn Admin commands must set this to 1.
      */
    def apply[Q,R](request: Q,
      fullCollectionName : String,
      requestId: Int,
      numberToReturn: Int = 0,
      numberToSkip: Int = 0,
      tailableCursor: Boolean = false,
      slaveOk: Boolean = false,
      noCursorTimeout: Boolean = false,
      exhaust: Boolean = false,
      partial: Boolean = false
    ): OP_QUERY[Q,R] = {
      val flags : BitVector = 
        ( BitVector.empty :+ 
          false :+
          tailableCursor :+
          slaveOk :+
          false :+
          noCursorTimeout :+
          exhaust :+
          partial ).padRight(32)

      new OP_QUERY[Q,R](
        MsgHeader(requestID = requestId),
        flags,
        fullCollectionName,
        numberToSkip,
        numberToReturn,
        request
      )
    }

    implicit def codec[Q : Codec, R: Projection]
        : Codec[OP_QUERY[Q,R]] = {
      import scodec.protocols.bson.bson
      messageLength("length" | int32, 
        (MsgHeader.codec 
          :: ("opcode" | constant(int32.encode(2004).require))
          :: ("flags" | bits(32).xmap[BitVector](_.reverseByteOrder.reverse,_.reverse.reverseByteOrder))
          :: ("collname" | cstring)
          :: ("numberToSkip" | int32)
          :: ("numberToReturn" | int32)
          :: ("request" | Codec[Q])
          :: ("returnFieldSelector" | bson(Projection[R].codec))
        ).dropUnits.as[OP_QUERY[Q,R]]
      ).withContext("OP_QUERY")
    }
  }

  /*
  /** TODO: Currently this only supports payload with homogeneous elements. */
  case class OP_MSG[Q : Codec, P : Codec](
    header: MsgHeader,
    flagBits: BitVector,
    sections: List[Body[Q] :+: Payload[P] :+: CNil], 
    //checksum: unit32
  )

  object OP_MSG {

    def apply[Q: Codec](body: Q, requestId: Int, moreToCome: Boolean = false) : OP_MSG[Q,Nothing] = {
      val flags = BitVector.low(32).update(1,moreToCome)
      new OP_MSG(
        MsgHeader(requestId),
        flags,
        List(Inl(Body(body)))
      )
    }

    def apply[Q: Codec, P: Codec](body: Q, identitfier: String, documents: List[P], requestId: Int, moreToCome: Boolean = false): OP_MSG[Q,P] = {
      val flags = BitVector.low(32).update(1,moreToCome)
      new OP_MSG(
        MsgHeader(requestId),
        flags,
        List(Inl(Body(body)),Inl(Inr(Payload[P](identifier, documents: _*))))
      )
    }

    case class Body[Q](document : Q)

    /** Note param `size` in the spec is implicitly added on encoding.
      * @param identifier Document sequence identifier. In all current commands this 
      *                   field is the (possibly nested) field that it is replacing from
      *                   the body section. This field MUST NOT also exist in the body 
      *                   section.
      * @param documents Objects are sequenced back to back with no separators.
      *                  Each object is limited to the maxBSONObjectSize of the server.
      *                  The combination of all objects is not limited to maxBSONObjSize.
      *                  The document sequence ends once size bytes have been consumed.
      *                  Parsers MAY choose to merge these objects into the body as an 
      *                  array at the path specified by the sequence identifier when 
      *                  converting to language-level objects.
      */
    case class Payload[P](
      identifier: String,
      document: P*
    )

    def codec[Q : Codec, P : Codec] : Codec[OP_MSG[Q,P]] = {
      val sectionCodec : Codec[Body[Q] :+: Payload[P] :+: CNil] = 
        (Codec[Body[Q]] :+: messageLength(int32,list(Codec[Payload[P]])))
          .discriminatedByIndex(byte)
        messageLength(int32, MsgHeader.codec :: (constant(2013) :~>: sectionCodec))
    }
  }
  */
}
