package mongosaur.mongodb

import scala.concurrent.duration._

/** Mongosaur write concern.
  * This augments MongoDB's write concern ([[_WriteConcern]])
  * with ordering and schema validation.
  */
case class WriteConcern(
  acks: Option[Int], //acks: Option[Either[Int,String]], //TODO: support "majority"
  journal: Option[Boolean],
  timeout: Option[FiniteDuration],
  ordered: Boolean,
  validate: Boolean
){
  /** Extracts the fields supported in the formal spec */
  def toMongoWriteConcern : _WriteConcern =
    _WriteConcern(
      acks,
      journal,
      timeout.map(_.toMillis.toInt)
    )
}

object WriteConcern {
  def default = WriteConcern(Some(1),None,None,false,false)
}

/** This is the native mongodb write concern, not meant for the user.
  * @see [[https://github.com/mongodb/specifications/blob/master/source/read-write-concern/read-write-concern.rst]]
  * @param j If true, wait for the the write operation to get committed
  *          to the journal. When unspecified, a driver MUST NOT send "j".
  * @param w When an integer, specifies the number of nodes that should
  *          acknowledge the write and MUST be greater than or equal to
  *          0.  When a string, indicates tags. "majority" is defined,
  *          but users could specify other custom error modes.
  *          When not specified, a driver MUST NOT send "w".
  * @param wtimeout If the write concern is not satisfied within the 
  *                 specified timeout (in milliseconds), the operation
  *                 will return an error.  The value MUST be greater 
  *                 than or equal to 0.  When not specified, a driver
  *                 should not send "wtimeout".
  */
case class _WriteConcern(
  w: Option[Int], //w: Option[Int,String]],  //TODO: support "majority"
  j: Option[Boolean],
  wtimeout: Option[Int]
)
