package mongosaur.mongodb

import scala.util.Try
import cats.instances.list._
import cats.instances.option._
import cats.syntax.traverse._

case class Version(major : Byte, minor : Byte, patch : Option[Byte] = None) {
  override def toString : String = 
    major.toString + "." + minor.toString + patch.map("." + _).getOrElse("")
}

object Version {
  /** Parses version from a string. */
  def parse(ver : String) : Option[Version] = 
    ver.split('.').toList.traverse { s => Try(s.toByte).toOption }.collectFirst{
      case major :: minor :: patch :: Nil => Version(major,minor,Some(patch))
      case major :: minor :: Nil => Version(major,minor)
    }

    
}

