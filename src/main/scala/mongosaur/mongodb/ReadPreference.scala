package mongosaur.mongodb

import scodec.Codec

import Client.{Topology,Server}
import fs2.io.tcp

/** @see [[https://github.com/mongodb/specifications/blob/master/source/server-selection/server-selection.rst#read-preference]] */
sealed trait ReadPreference {

  /** Message to be sent to `mongos`.
    * This is abstract as the fields to be sent vary depending on the
    * `mode`.
    * @see [[https://github.com/mongodb/specifications/blob/master/source/server-selection/server-selection.rst#passing-read-preference-to-mongos]]
    */
  protected trait _ReadPreference 

  /** Flag for `OP_QUERY`, to be sent to `mongos`. */
  def slaveOK : Boolean

  /** `\$readPreference` query modifier. */
  def toMongoReadPreference : _ReadPreference

  /** Returns a list of eligible servers, given a topology. */
  final def isEligible[F[_]](topology: Topology): List[(tcp.Socket[F],Server)] = {
    topology match {
      case s : Client.Topology.Single[F @ unchecked] => 
        s.server :: Nil
      case other => _isEligible(topology)
    }
  }

  protected def _isEligible[F[_]](topology: Topology)
    : List[(tcp.Socket[F],Server)]
}
  
object ReadPreference {

  case object Primary extends ReadPreference {

    trait ReadPreference {} 

    object _ReadPreference {
      implicit val codec: Codec[Unit] = ???
    }

    val slaveOK : Boolean = true

    val toMongoReadPreference = new _ReadPreference {}

    protected def _isEligible[F[_]](topology: Topology)
      : List[(tcp.Socket[F],Server)] = ???
  }

  /*
  case class Nearest[F[_]](
    tags: Set[String],
    maxStaleness: FiniteDuration
  ) extends NonDirect[F] {
    def _isEligible(topology: Client.Topology): List[Socket[F]] =
      topology match {
        case Toplogy.ReplicaSetWithPrimary => ???
      }
  } 
  */
}


