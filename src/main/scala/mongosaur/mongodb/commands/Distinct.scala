package mongosaur.mongodb.commands

/** Find distinct values in a collection.
  * @tparam K Type of `key` field, see [[Distinct.Reply]].
  * TODO: @tparam Q Type of the query filter.
  * @param distinct Collection name.
  * @param key Field name to find distinct values for.
  * @param query Query filter on collection.
  */
case class Distinct[K](
  distinct: String,
  key: String,
  //query: Option[Q] = None,
  //options: Distinct.Options
)

object Distinct {

  implicit def command[K] = Command[Distinct[K],Reply[K]]

  case class Reply[K](values: List[K])

  /*
  case class Options[C](
    collation: C
  )
  */
}
