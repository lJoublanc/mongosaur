package mongosaur.mongodb.commands

import cats.{Semigroup,SemigroupK}
import cats.instances.option._
import cats.syntax.semigroupk._

import mongosaur.ServerError
import mongosaur.mongodb._WriteConcern

/** @see [[https://docs.mongodb.com/manual/reference/command/insert/]] */
case class Insert[D](
  insert: String,
  documents: List[D],
  ordered: Boolean,
  writeConcern: _WriteConcern,
  bypassDocumentValidation: Boolean
)

object Insert {

  case class Reply(
    n: Int,
    writeErrors: Option[List[Reply.WriteError]] = None, 
    writeConcernErrors: Option[List[Reply.WriteConcernError]] = None,
    ok: Double = 1.0
  ) extends Command.Ok

  object Reply {
    /** This is not a monoid as we need to distinguish lack of a reply with a `Reply(0)`. */
    implicit val semigroupInstance: Semigroup[Reply] = 
      (a,b) => {
        val SK = SemigroupK[Option].compose[List]
        Reply( 
          a.n + b.n,
          a.writeErrors <+> b.writeErrors,
          a.writeConcernErrors <+> b.writeConcernErrors,
          if (a.ok == 1.0 && b.ok == 1.0) 1.0 else 0.0
        )
      }
    case class WriteError(index: Int, code: Int, errmsg: String) extends ServerError(s"at $index: ($code): $errmsg")
    case class WriteConcernError(code: Int, errmsg: String) extends ServerError(s"($code): $errmsg")
  }

  implicit def command[A] = Command[Insert[A],Reply]
}
