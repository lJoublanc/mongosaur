package mongosaur.mongodb

import mongosaur.ServerError

/** Predefined case-classes for standard MongoDB commands.
  *
  * To create your own, here is a vim command that helps convert JSON to a scala
  * case class.
  * {{{
  * :s/^\(\s*\)"\(\w\+\)":\s*<\(\w\+\)>,\(.*\)$/\1\2: \3\4/
  * }}}
  *
  * Note that all these are serialized into BSON, and as such they should ^not^
  * have a sealed BSON `Codec` in the companion (i.e. a top-level BSON document).
  * Otherwise this will mess up implicit derivation and cause SOs.
  * Unsealed `Codec`s are ok though.
  */
package object commands {

  /** Typeclass used to infer the reply of a command. Put an instance of this
    * into the companion of your case-class so it can be used as a command. Then
    * you can pass your case class into [[Client#command]].
    * @example {{{
    * case class Ping(ping: Int)
    *
    * case class Pong(ok: Double) extends Command.Ok
    *
    * object Ping {
    *   implicit val command = Command[Ping,Pong]
    * }
    * }}}
    */
  trait Command[Q] {
    type A
  }

  object Command {

    /** Aux pattern to infer command return type. */
    type Reply[Query,Answer] = Command[Query] { type A = Answer }

    /** Constructor for [[Command.Reply]] */
    def apply[Q, A0]: Reply[Q,A0] = new Command[Q] { type A = A0 }

    /** All replies to a command have an `ok` field embedded as the last element.
      * The companion is a case object which can be used for convenience for checking command success.
      * @see [[https://docs.mongodb.com/manual/reference/method/db.runCommand/#command-response]]
      */
    trait Ok {
      def ok: Double
      // def operationTime: Option[Instant] //TODO: Option[_] not implemented yet. Only for replica sets and  shared clusters.
    }

    case object Ok extends Ok {

      val ok = 1.0

      implicit val semigroupInstance: cats.Semigroup[Ok] = 
        (a,b) => if (a.ok == 1.0 && b.ok == 1.0) new Ok { val ok = 1.0 } else new Ok { val ok = 0.0 }
    }

    /** Returned by OP_REPLY when sending a command (i.e. with adming database
      * as the `fullCollectionName` argument), which results in failure.
      */
    case class Error(
      ok: Double,
      errmsg: String,
      code: Error.Code.Value,
    ) extends ServerError(s"(${code.id}) $errmsg") with Ok

    object Error {
      import scodec.protocols.bson._
      import scodec.Attempt
      import scodec.codecs.{~,ValueEnrichedWithTuplingSupport}
      import shapeless.Witness

      /** Note this is unsealed for inference to work. */
      private val enumCodec = 
        (int32[Witness.`'code`.T] ~ utf8[Witness.`'codeName`.T])
          .narrowc[Error.Code.Value]{
            case id ~ str =>
              Attempt successful Error.Code(id)
          }{ code => 
            code.id ~ code.toString
          }

      val codec = 
        bson(double[Witness.`'ok`.T] :: utf8[Witness.`'errmsg`.T] :: enumCodec)
          .as[Error]

      object Code extends Enumeration {
        val OK = Value(0)
        val InternalError = Value(1)
        val BadValue = Value(2)
        val OBSOLETE_DuplicateKey = Value(3)
        val NoSuchKey = Value(4)
        val GraphContainsCycle = Value(5)
        val HostUnreachable = Value(6)
        val HostNotFound = Value(7)
        val UnknownError = Value(8)
        val FailedToParse = Value(9)
        val CannotMutateObject = Value(10)
        val UserNotFound = Value(11)
        val UnsupportedFormat = Value(12)
        val Unauthorized = Value(13)
        val TypeMismatch = Value(14)
        val Overflow = Value(15)
        val InvalidLength = Value(16)
        val ProtocolError = Value(17)
        val AuthenticationFailed = Value(18)
        val CannotReuseObject = Value(19)
        val IllegalOperation = Value(20)
        val EmptyArrayOperation = Value(21)
        val InvalidBSON = Value(22)
        val AlreadyInitialized = Value(23)
        val LockTimeout = Value(24)
        val RemoteValidationError = Value(25)
        val NamespaceNotFound = Value(26)
        val IndexNotFound = Value(27)
        val PathNotViable = Value(28)
        val NonExistentPath = Value(29)
        val InvalidPath = Value(30)
        val RoleNotFound = Value(31)
        val RolesNotRelated = Value(32)
        val PrivilegeNotFound = Value(33)
        val CannotBackfillArray = Value(34)
        val UserModificationFailed = Value(35)
        val RemoteChangeDetected = Value(36)
        val FileRenameFailed = Value(37)
        val FileNotOpen = Value(38)
        val FileStreamFailed = Value(39)
        val ConflictingUpdateOperators = Value(40)
        val FileAlreadyOpen = Value(41)
        val LogWriteFailed = Value(42)
        val CursorNotFound = Value(43)
        val UserDataInconsistent = Value(45)
        val LockBusy = Value(46)
        val NoMatchingDocument = Value(47)
        val NamespaceExists = Value(48)
        val InvalidRoleModification = Value(49)
        val MaxTimeMSExpired = Value(50)
        val ManualInterventionRequired = Value(51)
        val DollarPrefixedFieldName = Value(52)
        val InvalidIdField = Value(53)
        val NotSingleValueField = Value(54)
        val InvalidDBRef = Value(55)
        val EmptyFieldName = Value(56)
        val DottedFieldName = Value(57)
        val RoleModificationFailed = Value(58)
        val CommandNotFound = Value(59)
        val OBSOLETE_DatabaseNotFound = Value(60)
        val ShardKeyNotFound = Value(61)
        val OplogOperationUnsupported = Value(62)
        val StaleShardVersion = Value(63)
        val WriteConcernFailed = Value(64)
        val MultipleErrorsOccurred = Value(65)
        val ImmutableField = Value(66)
        val CannotCreateIndex = Value(67)
        val IndexAlreadyExists = Value(68)
        val AuthSchemaIncompatible = Value(69)
        val ShardNotFound = Value(70)
        val ReplicaSetNotFound = Value(71)
        val InvalidOptions = Value(72)
        val InvalidNamespace = Value(73)
        val NodeNotFound = Value(74)
        val WriteConcernLegacyOK = Value(75)
        val NoReplicationEnabled = Value(76)
        val OperationIncomplete = Value(77)
        val CommandResultSchemaViolation = Value(78)
        val UnknownReplWriteConcern = Value(79)
        val RoleDataInconsistent = Value(80)
        val NoMatchParseContext = Value(81)
        val NoProgressMade = Value(82)
        val RemoteResultsUnavailable = Value(83)
        val DuplicateKeyValue = Value(84)
        val IndexOptionsConflict = Value(85)
        val IndexKeySpecsConflict = Value(86)
        val CannotSplit = Value(87)
        val SplitFailed_OBSOLETE = Value(88)
        val NetworkTimeout = Value(89)
        val CallbackCanceled = Value(90)
        val ShutdownInProgress = Value(91)
        val SecondaryAheadOfPrimary = Value(92)
        val InvalidReplicaSetConfig = Value(93)
        val NotYetInitialized = Value(94)
        val NotSecondary = Value(95)
        val OperationFailed = Value(96)
        val NoProjectionFound = Value(97)
        val DBPathInUse = Value(98)
        val UnsatisfiableWriteConcern = Value(100)
        val OutdatedClient = Value(101)
        val IncompatibleAuditMetadata = Value(102)
        val NewReplicaSetConfigurationIncompatible = Value(103)
        val NodeNotElectable = Value(104)
        val IncompatibleShardingMetadata = Value(105)
        val DistributedClockSkewed = Value(106)
        val LockFailed = Value(107)
        val InconsistentReplicaSetNames = Value(108)
        val ConfigurationInProgress = Value(109)
        val CannotInitializeNodeWithData = Value(110)
        val NotExactValueField = Value(111)
        val WriteConflict = Value(112)
        val InitialSyncFailure = Value(113)
        val InitialSyncOplogSourceMissing = Value(114)
        val CommandNotSupported = Value(115)
        val DocTooLargeForCapped = Value(116)
        val ConflictingOperationInProgress = Value(117)
        val NamespaceNotSharded = Value(118)
        val InvalidSyncSource = Value(119)
        val OplogStartMissing = Value(120)
        val DocumentValidationFailure = Value(121) // Only for the document validator on collections.
        val OBSOLETE_ReadAfterOptimeTimeout = Value(122)
        val NotAReplicaSet = Value(123)
        val IncompatibleElectionProtocol = Value(124)
        val CommandFailed = Value(125)
        val RPCProtocolNegotiationFailed = Value(126)
        val UnrecoverableRollbackError = Value(127)
        val LockNotFound = Value(128)
        val LockStateChangeFailed = Value(129)
        val SymbolNotFound = Value(130)
        val RLPInitializationFailed = Value(131)
        val OBSOLETE_ConfigServersInconsistent = Value(132)
        val FailedToSatisfyReadPreference = Value(133)
        val ReadConcernMajorityNotAvailableYet = Value(134)
        val StaleTerm = Value(135)
        val CappedPositionLost = Value(136)
        val IncompatibleShardingConfigVersion = Value(137)
        val RemoteOplogStale = Value(138)
        val JSInterpreterFailure = Value(139)
        val InvalidSSLConfiguration = Value(140)
        val SSLHandshakeFailed = Value(141)
        val JSUncatchableError = Value(142)
        val CursorInUse = Value(143)
        val IncompatibleCatalogManager = Value(144)
        val PooledConnectionsDropped = Value(145)
        val ExceededMemoryLimit = Value(146)
        val ZLibError = Value(147)
        val ReadConcernMajorityNotEnabled = Value(148)
        val NoConfigMaster = Value(149)
        val StaleEpoch = Value(150)
        val OperationCannotBeBatched = Value(151)
        val OplogOutOfOrder = Value(152)
        val ChunkTooBig = Value(153)
        val InconsistentShardIdentity = Value(154)
        val CannotApplyOplogWhilePrimary = Value(155)
        val OBSOLETE_NeedsDocumentMove = Value(156)
        val CanRepairToDowngrade = Value(157)
        val MustUpgrade = Value(158)
        val DurationOverflow = Value(159)
        val MaxStalenessOutOfRange = Value(160)
        val IncompatibleCollationVersion = Value(161)
        val CollectionIsEmpty = Value(162)
        val ZoneStillInUse = Value(163)
        val InitialSyncActive = Value(164)
        val ViewDepthLimitExceeded = Value(165)
        val CommandNotSupportedOnView = Value(166)
        val OptionNotSupportedOnView = Value(167)
        val InvalidPipelineOperator = Value(168)
        val CommandOnShardedViewNotSupportedOnMongod = Value(169)
        val TooManyMatchingDocuments = Value(170)
        val CannotIndexParallelArrays = Value(171)
        val TransportSessionClosed = Value(172)
        val TransportSessionNotFound = Value(173)
        val TransportSessionUnknown = Value(174)
        val QueryPlanKilled = Value(175)
        val FileOpenFailed = Value(176)
        val ZoneNotFound = Value(177)
        val RangeOverlapConflict = Value(178)
        val WindowsPdhError = Value(179)
        val BadPerfCounterPath = Value(180)
        val AmbiguousIndexKeyPattern = Value(181)
        val InvalidViewDefinition = Value(182)
        val ClientMetadataMissingField = Value(183)
        val ClientMetadataAppNameTooLarge = Value(184)
        val ClientMetadataDocumentTooLarge = Value(185)
        val ClientMetadataCannotBeMutated = Value(186)
        val LinearizableReadConcernError = Value(187)
        val IncompatibleServerVersion = Value(188)
        val PrimarySteppedDown = Value(189)
        val MasterSlaveConnectionFailure = Value(190)
        val OBSOLETE_BalancerLostDistributedLock = Value(191)
        val FailPointEnabled = Value(192)
        val NoShardingEnabled = Value(193)
        val BalancerInterrupted = Value(194)
        val ViewPipelineMaxSizeExceeded = Value(195)
        val InvalidIndexSpecificationOption = Value(197)
        val OBSOLETE_ReceivedOpReplyMessage = Value(198)
        val ReplicaSetMonitorRemoved = Value(199)
        val ChunkRangeCleanupPending = Value(200)
        val CannotBuildIndexKeys = Value(201)
        val NetworkInterfaceExceededTimeLimit = Value(202)
        val ShardingStateNotInitialized = Value(203)
        val TimeProofMismatch = Value(204)
        val ClusterTimeFailsRateLimiter = Value(205)
        val NoSuchSession = Value(206)
        val InvalidUUID = Value(207)
        val TooManyLocks = Value(208)
        val StaleClusterTime = Value(209)
        val CannotVerifyAndSignLogicalTime = Value(210)
        val KeyNotFound = Value(211)
        val IncompatibleRollbackAlgorithm = Value(212)
        val DuplicateSession = Value(213)
        val AuthenticationRestrictionUnmet = Value(214)
        val DatabaseDropPending = Value(215)
        val ElectionInProgress = Value(216)
        val IncompleteTransactionHistory = Value(217)
        val UpdateOperationFailed = Value(218)
        val FTDCPathNotSet = Value(219)
        val FTDCPathAlreadySet = Value(220)
        val IndexModified = Value(221)
        val CloseChangeStream = Value(222)
        val IllegalOpMsgFlag = Value(223)
        val QueryFeatureNotAllowed = Value(224)
        val TransactionTooOld = Value(225)
        val AtomicityFailure = Value(226)
        val CannotImplicitlyCreateCollection = Value(227)
        val SessionTransferIncomplete = Value(228)
        val MustDowngrade = Value(229)
        val DNSHostNotFound = Value(230)
        val DNSProtocolError = Value(231)
        val MaxSubPipelineDepthExceeded = Value(232)
        val TooManyDocumentSequences = Value(233)
        val RetryChangeStream = Value(234)
        val InternalErrorNotSupported = Value(235) // this function or module is not available on this platform or configuration
        val ForTestingErrorExtraInfo = Value(236)
        val CursorKilled = Value(237)
        val NotImplemented = Value(238)
        val SnapshotTooOld = Value(239)
        val DNSRecordTypeMismatch = Value(240)
        val ConversionFailure = Value(241)
        val CannotCreateCollection = Value(242)
        val IncompatibleWithUpgradedServer = Value(243)
        val NOT_YET_AVAILABLE_TransactionAborted = Value(244)
        val BrokenPromise = Value(245)
        val SnapshotUnavailable = Value(246)
        val ProducerConsumerQueueBatchTooLarge = Value(247)
        val ProducerConsumerQueueEndClosed = Value(248)
        val StaleDbVersion = Value(249)
        val StaleChunkHistory = Value(250)
        val NoSuchTransaction = Value(251)
        val ReentrancyNotAllowed = Value(252)
        val FreeMonHttpInFlight = Value(253)
        val FreeMonHttpTemporaryFailure = Value(254)
        val FreeMonHttpPermanentFailure = Value(255)
        val TransactionCommitted = Value(256)
        val TransactionTooLarge = Value(257)
        val UnknownFeatureCompatibilityVersion = Value(258)
        val KeyedExecutorRetry = Value(259)
        val InvalidResumeToken = Value(260)
        val TooManyLogicalSessions = Value(261)
        val ExceededTimeLimit = Value(262)
        val OperationNotSupportedInTransaction = Value(263)
        val TooManyFilesOpen = Value(264)
        val OrphanedRangeCleanUpFailed = Value(265)
        val FailPointSetFailed = Value(266)
        val PreparedTransactionInProgress = Value(267)
        val CannotBackup = Value(268)
        val DataModifiedByRepair = Value(269)
        val RepairedReplicaSetNode = Value(270)
        // TODO SERVER-36385: Mark LongTypeBits as obsolete in 4.4: we don't need
        // to set the feature tracker bit for long TypeBits in 4.4 given that we
        // can't downgrade 4.4 to a version (such as 4.0) which cannot read long
        // TypeBits.
        val KeyStringWithLongTypeBits = Value(271)
        // Error codes 4000-8999 are reserved.

        // Non-sequential error codes (for compatibility only)
        val SocketException = Value(9001)
        val OBSOLETE_RecvStaleConfig = Value(9996)
        val NotMaster = Value(10107)
        val CannotGrowDocumentInCappedNamespace = Value(10003)
        val BSONObjectTooLarge = Value(10334)
        val DuplicateKey = Value(11000)
        val InterruptedAtShutdown = Value(11600)
        val Interrupted = Value(11601)
        val InterruptedDueToStepDown = Value(11602)
        val OutOfDiskSpace = Value(14031)
        // TODO SERVER-36385: Mark KeyTooLong error as obsolete
        val KeyTooLong = Value(17280)
        val BackgroundOperationInProgressForDatabase = Value(12586)
        val BackgroundOperationInProgressForNamespace = Value(12587)
        val NotMasterOrSecondary = Value(13436)
        val NotMasterNoSlaveOk = Value(13435)
        val ShardKeyTooBig = Value(13334)
        val StaleConfig = Value(13388)
        val DatabaseDifferCase = Value(13297)
        val OBSOLETE_PrepareConfigsFailed = Value(13104)
      }
    }
  }
}
