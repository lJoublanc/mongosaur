package mongosaur.mongodb.commands

import shapeless.{HList,LUBConstraint}

/** @param createIndexes The name of the collection. */
sealed abstract case class CreateIndexes[L <: HList] private (
  createIndexes: String,
  indexes: L
)

object CreateIndexes {

  def apply[L <: HList](createIndexes: String, indexes: L)(
      implicit isIdx: LUBConstraint[L,Index[_]]): CreateIndexes[L] =
    new CreateIndexes[L](createIndexes,indexes) {}

  case class Reply(
    createdCollectionAutomatically: Boolean,
    numIndexesBefore: Int,
    numIndexesAfter: Int,
    note: Option[String],
    ok: Double)
  extends Command.Ok

  implicit def command[L <: HList] = Command[CreateIndexes[L],Reply]

  /** An index in ascending order. */
  final val ascending : Int = 1

  /** An index in descending order. */
  final val descending: Int = -1

  sealed trait Index[K] {
    val name: String
    val key: K
    val background: Boolean
    val expireAfterSeconds: Option[Int],
  }
  
  /** Create one or more on indices on the given fields in the current collection.
    * `key` is a case class where the fields correspond to document field names, 
    * and their values are, either an integer [[ascending]]/ [[descending]],
    * signifying sort order, or a string, signifying the index type. Allowed types:
    * <ul>
    *   </li> `hashed`
    *   </li> `text`
    *   </li> `2d`
    *   </li> `2dShpere`
    *   </li> `geoHaystack`
    * </ul>
    *
    * TODO: proper constraints on field types and values (use scala.Symbol?)
    *
    * @param K a case class of index keys.
    * @param name The name of the index.
    * @param key each field in `key` must take a string or int value, see above.
    * @see [[https://docs.mongodb.com/v3.4/reference/command/createIndexes/#dbcmd.createIndexes]]
    */
  case class LinearIndex[K <: Product](
    name: String,
    key: K,
    background: Boolean = false,
    unique: Boolean = false,
    expireAfterSeconds: Option[Int] = None,
    /*
    partialFilterExpression: E, //TODO: partialFilterExpression "preferred over 'sparse'"
    storageEngine: D //TODO: storageengine
    collation: C //TODO: collation
    */
  ) extends Index[K]

  case class TextIndex[K <: Product](
    name: String,
    key: K,
    background: Boolean = false,
    unique: Boolean = false,
    expireAfterSeconds: Option[Int] = None,
    weights: Int = 1,
    default_language: String = "english",
    language_override: String = "language",
    textIndexVersion: Int = 3
  ) extends Index[K]

  case class TwoDSphereIndex[K <: Product](
    name: String,
    key: K,
    background: Boolean = false,
    unique: Boolean = false,
    expireAfterSeconds: Option[Int] = None,
    `2dsphereIndexVersion`: Int = 3
  ) extends Index[K]

  case class TwoDIndex[K <: Product](
    name: String,
    key: K,
    background: Boolean = false,
    unique: Boolean = false,
    expireAfterSeconds: Option[Int] = None,
    bits: Int = 26,
    min: Float = -180.0f,
    max: Float = 180.0f
  ) extends Index[K]

  case class GeoHaystackIndex[K <: Product](
    name: String,
    key: K,
    background: Boolean = false,
    unique: Boolean = false,
    expireAfterSeconds: Option[Int] = None,
    bucketSize: Option[Float] = None
  ) extends Index[K]

  /** Does not support multiple keys.
    * @tparam K A singleton case-class, with `"hashed"` as the second element.
    */
  case class HashedIndex[K <: Product1[String]](
    name: String,
    key: K,
    background: Boolean = false,
    expireAfterSeconds: Option[Int] = None
  ) extends Index[K]
}
