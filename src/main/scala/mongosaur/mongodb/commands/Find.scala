package mongosaur.mongodb.commands

/** An alternative to [[mongosaur.mongodb.Collection#find]] that allows
  * fine grained control of the query.
  * TODO: at moment there is lots of boilerplate required to use this. You
  * need to define case classes for Query,Sort,Projection.
  * You will also need to handle OP_MORE etc. if you want to get more than
  * a single page of results.
  * @see [[mongosaur.mongodb.Collection#find]], for a light-weight
  *      alternative. 
  * @example {{{
  * import scodec.protocols.bson.implicits._
  * case class Document(x: Int)
  * case class Gt(`$gt`: Int)
  * case class Query(_id: Gt)
  * case class Sort(x: Int)
  * case class Projection(_id: Int = 0, x: Int = 1)
  *
  * implicit val cmd = Command[Find[Query,Sort,Projection],Find.Reply[Document]]
  * coll.command(
  *   Find(
  *     find = coll.name,
  *     filter = Some(Query(_id = Gt(1))),
  *     sort = Option.empty[Sort], //note explicit type Option[Sort] rather than None.
  *     projection = Some(Projection())
  *   )
  * )
  * }}}
  */
case class Find[Q,S,P](
  find: String,
  filter: Option[Q],
  sort: Option[S],
  projection: Option[P],
  hint: Option[String] = None, // Specify either the index name as a string TODO: or the index key pattern
  skip: Option[Int] = None,
  limit: Option[Int] = None,
  batchSize: Option[Int] = None,
  singleBatch: Option[Boolean] = None,
  comment: Option[String] = None,
  maxScan: Option[Int] = None,
  maxTimeMS: Option[Int] = None,
  //TODO: readConcern - isn't this set at the wire level?
  max: Option[Int] = None,
  min: Option[Int] = None
) 

object Find {
  /*
import scodec.Codec
import scodec.protocols.bson._
import shapeless.Witness
import mongosaur.Projection

  //TODO: infer projection from return type. This is complex as we want to
  //override the projection at the document level, NOT at the OP_QUERY level.

  implicit def codec[Q,S,A](implicit 
      projection: Projection[A],
      Q: BSONElement[Q],
      S: BSONElement[S]): Codec[Find[Q,S,A]] =
    bson {
      utf8[Witness.`'find`.T] ::
      option[Witness.`'filter`.T](Q) ::
      option[Witness.`'sort`.T](S) ::
      document[Witness.`'projection`.T](projection.codec) ::
      option[Witness.`'hint`.T](utf8) ::
      option[Witness.`'skip`.T](int32) ::
      option[Witness.`'limit`.T](int32) ::
      option[Witness.`'batchSize`.T](int32) ::
      option[Witness.`'singleBatch`.T](boolean) ::
      option[Witness.`'comment`.T](utf8) ::
      option[Witness.`'maxScan`.T](int32) ::
      option[Witness.`'maxTimeMS`.T](int32) ::
      option[Witness.`'max`.T](int32) ::
      option[Witness.`'min`.T](int32)
    }.dropUnits.as[Find[Q,S,A]]
  */
      
  case class Reply[A](cursor: Reply.Cursor[A], ok: Double) extends Command.Ok
  object Reply {
    //TODO: when non-empty, firstBatch is returned as the first element. When empty, as the last :(
    //One possible soln is to exclude id/ns in the results.
    case class Cursor[A](firstBatch: List[A], id: Long, ns: String)
  }
}
