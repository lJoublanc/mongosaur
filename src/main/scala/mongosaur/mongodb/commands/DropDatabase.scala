package mongosaur.mongodb.commands

/** Removes a database. */
case class DropDatabase(dropDatabase: Boolean = true)
