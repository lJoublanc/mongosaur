package mongosaur.mongodb.commands

import mongosaur.mongodb.WireVersion

import java.time.Instant

/** A special version of [[IsMaster]] that is issued as the first message.
  * @param saslSupportedMechs <dbname.username>
  * @see [[https://github.com/mongodb/specifications/blob/master/source/auth/auth.rst#authentication-handshake]]
  */
case class Handshake(
  isMaster : Boolean,
  //saslSupportedMechs : String,
  client : Handshake.Client
)

object Handshake {
  case class Client(
    application : Client.Application,
    driver : Client.Driver,
    os : Client.Os,
    platform : String
  )

  object Client{
    case class Application(name: String)
    case class Driver(name: String, version: String)
    case class Os(`type`: String, name: String, architecture: String, version: String)
  }

  implicit val command = Command[Handshake,IsMaster.Standalone]
}

/** @see [[https://docs.mongodb.com/manual/reference/command/isMaster/]] */
case class IsMaster(
  isMaster : Boolean,
  //saslSupportedMechs : String
)

object IsMaster {

  sealed trait Reply 
  extends Product 
  with Serializable 
  with Command.Ok 
  with WireVersion {
    def ismaster: Boolean

    def maxBsonObjectSize: Int

    def maxMessageSizeBytes: Int

    def maxWriteBatchSize: Int

    def localTime: Instant

    val maxWireVersion: Int

    val minWireVersion: Int

    /** @since 3.6 */
    //def logicalSessionTimeoutMinutes: Option[Int]

    def readOnly: Boolean

    //def compression: Option[List[String]]

    //saslSupportedMechs: Array[String]

    def ok: Double
  }

  final case class Standalone(
    ismaster: Boolean,
    maxBsonObjectSize: Int,
    maxMessageSizeBytes: Int,
    maxWriteBatchSize: Int,
    localTime: Instant,
    //logicalSessionTimeoutMinutes: Option[Int],
    maxWireVersion: Int,
    minWireVersion: Int,
    readOnly: Boolean,
    //compression: Option[List[String]],
    //saslSupportedMechs: Array[String]
    ok: Double,
  ) extends Reply

  implicit val command = Command[IsMaster,Standalone]
}
