package mongosaur.mongodb.commands

/** Removes a collection. 
  * @param drop The collection name.
  */
case class Drop(drop: String)

object Drop {
  case class Reply(ns: String, nIndexesWas: Int, ok: Double) extends Command.Ok

  implicit val command = Command[Drop,Reply]
}
