package mongosaur

import cats.Semigroup
import cats.effect._
import fs2.Stream
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.Assertion
import org.scalatest.Succeeded
import scodec.protocols.bson.implicits._

import mongosaur.mongodb._
import mongosaur.mongodb.commands._

import scala.concurrent.ExecutionContext.Implicits.global

class CRUD extends FlatSpec with Matchers {

  implicit val timer: Timer[IO] = IO timer global

  implicit val ctxShift: ContextShift[IO] = IO contextShift global

  /* ScalaTest failed assertions return a `Nothing`, so there is only a concept
   * of a `Succeeded` test
   */
  implicit val assertionMonoidInstance: Semigroup[Assertion]  =
    (a: Assertion, b: Assertion) => (a,b) match {
      case (Succeeded,Succeeded) => Succeeded
      case _ => throw new Error("Impossible")
    }

  val cs = ConnectionString()

  case class Document(_id: Int, x: Int)

  /** Folds the resulting stream with semigroup, and retuns `Some(Assertion)` if 
    * one or more elements were emitted. Returns `None` if no elements were emitted.
    */
  def foldWithTestColl(
    f: Collection[IO] => Stream[IO,Assertion]): Assertion = {
    for { cli <- Client[IO](cs)
          db = cli.database("test")
          coll = db.collection("test")
          a <- f(coll)
    } yield a
  }.compile.foldSemigroup.map {
    case Some(a) => a
    case None => throw new Exception("No elements were returned.")
  }.unsafeRunSync

  "insert one" should "insert a single document" in {
    foldWithTestColl { coll =>
      for { _ <- coll.drop.handleErrorWith {
                   case Command.Error(0,_,Command.Error.Code.NamespaceNotFound) => 
                     Stream({})
                 }
            i <- coll insert Document(_id = 1, x = 11) }
       yield i shouldEqual Insert.Reply(1)
    } 
  }

  "create index" should "create an index" in foldWithTestColl { coll =>
      import commands.CreateIndexes
      import commands.CreateIndexes.{LinearIndex,ascending}
      import shapeless.HNil

      case class Keys(x: Int)

      val indices = LinearIndex("testIndex", Keys(x = ascending)) :: HNil

      coll.command(CreateIndexes(coll.name,indices))
        .covaryOutput[Command.Ok]
        .map(_.ok shouldEqual Command.Ok.ok) >> {

        coll.command(CreateIndexes(coll.name,indices))
          .map(_.note should not be empty) //message saying duplicate index.
      }
    }

  "insert many" should "insert multiple documents" in {
    import cats.instances.list._
    foldWithTestColl { coll =>
      val docs = List.tabulate(6)(i => Document(i,i * 11)).drop(2)
      coll insert docs map (_ shouldEqual Insert.Reply(4))
    }
  }

  "find" should "find with filter" in {
    case class Query(_id: Int)

    foldWithTestColl { coll =>
      for (a <- coll.find[Document](Query(_id = 1)) ) 
        yield a shouldEqual Document(1,11)
    }
  }

  it should "only return requested fields only" in {
    case class Query(_id: Int)
    case class Document(x: Int)

    foldWithTestColl { coll =>
      for (a <- coll.find[Document](Query(_id = 1)) ) 
        yield a shouldEqual Document(11)
    }
  }

  it should "project when using commands/Find" in {
    import scodec.protocols.bson.implicits._
    case class Document(x: Int)
    case class Gt(`$gt`: Int)
    case class Query(_id: Gt)
    case class Sort(x: Int)
    case class Projection(_id: Int = 0, x: Int = 1)

    foldWithTestColl { coll =>
      implicit val cmd = Command[Find[Query,Sort,Projection],Find.Reply[Document]]
      coll.command(
        Find(
          find = coll.name,
          filter = Some(Query(_id = Gt(1))),
          sort = Option.empty[Sort],
          projection = Some(Projection())
        )
      ).map(_.cursor.firstBatch shouldEqual List.tabulate(6)(i => Document(i * 11)).drop(2))
    }
  }

  "distinct" should "return all documents" in foldWithTestColl { coll =>
    coll.command(Distinct[Int](coll.name,"x"))
      .map(_.values shouldEqual (1 to 5).toList.map(_ * 11))
  }
}
