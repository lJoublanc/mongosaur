package mongosaur.mongodb

import java.nio.channels.AsynchronousChannelGroup 
import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext.Implicits.global

import cats.effect._
import scodec.protocols.bson.implicits._

import commands.Command

object PingPong extends IOApp {
  def run(args : List[String]) : IO[ExitCode] = {
    implicit val acg = AsynchronousChannelGroup.withThreadPool(Executors.newCachedThreadPool)

    case class Ping(ping: Int)

    object Ping {
      implicit val command = Command[Ping,Pong]
    }

    case class Pong(ok: Double) extends Command.Ok

    val program = for { cli <- Client[IO](ConnectionString())
                        pong <- cli.command(Ping(1))
                  } yield pong.ok.toByte

    program.compile.last.map(_.fold(ExitCode(-1))(i => ExitCode(i - 1 toByte)) )
  }
}
