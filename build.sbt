name := "mongosaur"

description := "A Scala MongoDB Client"

organization := "com.dinogroup"

scalaVersion := "2.12.7"

// This sets the version. To override locally, create a version.sbt file.
enablePlugins(GitVersioning)

licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

bintrayRepository := "dinogroup"

bintrayOrganization := Some("dmbl")

resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.7")

libraryDependencies ++= Seq(
  "com.dinogroup" %% "scodec-bson" % "0.2.3",
  "co.fs2" %% "fs2-core" % "1.0.0",
  "co.fs2" %% "fs2-io" % "1.0.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
  )

scalacOptions ++= Seq(
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  "-Ypartial-unification",
  "-Ywarn-value-discard",
  "-Ywarn-unused-import"
  )

scalacOptions in (Compile, console) ~= {
  _.filterNot("-Ywarn-unused-import" == _)
   .filterNot("-Xlint" == _)
   .filterNot("-Xfatal-warnings" == _)
}

initialCommands in (Compile, console) := """
  import fs2.Stream
  import scodec.Codec
  import cats.effect._
  import scala.concurrent.ExecutionContext.Implicits.global
  import mongosaur.mongodb._
"""

initialCommands in (Compile, consoleQuick) := """
  import fs2.Stream
  import scodec.Codec
  import scodec.protocols.bson.implicits._
  import cats.effect._
  import scala.concurrent.ExecutionContext.Implicits.global
"""
