# Mongosaur

A MongoDB client for scala, built on the typelevel stack. 

Copyright (c) 2018 Dinosaur Merchant Bank Limited  

Author: Luciano Joublanc

## Quickstart

To use in your application, add the following line to your `build.sbt`:

    resolvers += Resolver.bintrayRepo("dmbl","dinogroup")
    libraryDependencies += "com.dinogroup" %% "mongosaur" % "0.1.2"

The following writes multiple records to `test.weather`, and then reads them back into a vector.
You can run this from the SBT console, using `:paste`.

```scala
import mongosaur.mongodb._
import cats.effect._
import java.time.Instant
import cats.instances.vector._ //needed for insert
import scodec.protocols.bson.implicits._

implicit val timer: Timer[IO] = IO timer global

implicit val ctxShift: ContextShift[IO] = IO contextShift global

case class Weather(
  city: String, 
  timestamp: Instant,
  temp: Double,
  humidity: Double)

case class Query(temp: Warm)
case class Warm(`$gt`: Double)

val weather = 
  Vector.tabulate (100) { t => 
    Weather("London",
      Instant ofEpochMilli t, 
      temp = 19.0 + math.random(),
      humidity = 50.0 + math.random()
    )
  }

val program = 
  for { cli <- Client[IO](ConnectionString())
        db = cli.database("test")
        coll = db.collection("weather")
        _ <- coll.insert(weather)
        day <- coll.find[Weather](Query(temp = Warm(`$gt` = 19.5)))
  } yield day

program
  .compile
  .toVector
  .attempt
  .map { d =>
    println("List of warm days:\n" + d) ; ExitCode(0)
  }
```

## Features

Only a core set of features have been implemented. This project is a dependency for [scarctic](http://gitlab.com/lJoublanc/scarctic), and has been largely driven by it's requirements.

* Uses `scodec-bson` to provide type-safe, pre-compiled serializers.
* Uses `fs2` to provide resource-safety and asynchronicity.

You should be aware of the following limitations:

* Only single server topology is currently supported. #13
* No authentication #6
* Driver is currently synchronous, limiting throughput. #15
* Requires server version >= 3.4 (only tested on 3.4).

Also, you should be aware of the limitiation of the `scodec` approach. This requires us to have knowledge a-priori of schemas used within a collection. In practice this means that a call to `collection.find[A]` returns an `A`, which the user must specify. Another quirk of this is that field labels should be deterministic; this is often not the case, if you are connecting to an existing database.

## Contributing

Contributions are welcome. The first three missing features mentioned above will prevent the project from gaining wide-spread adoption, so if you want a challenge, start with those. If you're looking for a simple issue to start with, instead, see

* Implemented missing server commands #14
